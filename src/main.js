import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

import App from './App.vue'
import ResultForm from './Form.vue'
import Configure from './configure/Configure.vue'
import RawJson from './RawJson.vue'
import store from './store.js'

import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon.vue'

Vue.component('icon', Icon)
import axios from 'axios';
global.axios = axios;

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(Vuetify)

const stateStore = new Vuex.Store(store)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      component: ResultForm
    },
    {
      path: '/view',
      component: ResultForm
    },
    {
      path: '/json',
      component: RawJson
    }, 
    {
      path: '/configure',
      component: Configure
    }
  ]
})

new Vue({
  el: '#app',
  store: stateStore,
  router
}).$mount('#app')

console.log('this a page');