import Form from './DynamicForm.vue'
import {flatFields, validateSchema} from './util/schemableConfig.js'
import WitCombo from './fields/Combo.vue'

export {
    WitCombo,
    Form,
    flatFields,
    validateSchema
}

export default Form