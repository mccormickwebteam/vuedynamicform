/**
 * 
 * validation routines for checking values, results are used for front end display of warnings
 
 * 
 */

export function validateFields(schema, values = {}) {
    const validationResults = {};
    Object.keys(schema).forEach(function(key) {
        const col = schema[key];
        const value = values['schemable__'+col.key];

        validationResults[key] = validateField(col, value)
    })

    return validationResults
}

export function validateField(config, value) {
    let invalid = false;
    let errorMessage = '';

    if (config.required) {
        if (!value && value !== 0) {
            invalid = true;
            errorMessage = 'Field is required';
        }
    }
    
    if (config.maxlength) { 
        if (value && value.length > config.maxlength) {
            invalid = true;
            errorMessage = 'Max length is ' + config.maxlength;
        }
    } 
    if (config.minlength) { 
        if (!value || value.length < config.minlength) {
            invalid = true;
            errorMessage = 'Min length is ' + config.minlength;
        }
    }    

    if (config.maxvalue === 0 || config.maxvalue) { 
        if (value > config.maxvalue) {
            invalid = true;
            errorMessage = 'Maximum value is ' + config.maxvalue;
        }
    } 
    if (config.minvalue === 0 || config.minvalue) { 
        if (value < config.minvalue) {
            invalid = true;
            errorMessage = 'Minimum value is ' + config.minvalue;
        }
    }

    return {
        valid: !invalid,
        errorMessage: errorMessage
    }

}