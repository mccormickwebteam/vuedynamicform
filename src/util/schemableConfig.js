import uuid from 'uuid/v4'
import axios from 'axios';

/** 
 * s takes the schema format and returns an object hash keyed by uuid
 *
 */
export function flatFields(config) {
    // const display = config.display || [[]];
    // fields holds some more definition of the fields.
    // const fields = config.fields;
    const flatFields = [];
    const fieldHash = {}

   // console.log(config)

    config.forEach((question) => {
        if ( question.active ) {
            
            const fld = {
                id: question.id,
                type: "",
                required: question.required,
                label: question.name,
                key: question.id,
                values: question.options,
                optionsAPI: question.optionsAPI,
                note: question.note,
                maxlength: question.maximum_length,
                minlength: question.minimum_length,
                apiUrl: question.apiURL,
                api_fact: question.api_fact,
                columns: question.column_definitions,
                create: question.create,
                copy: question.copy,
                update: question.update,
                delete: question.delete
            }

            //console.log(fld)
            
            if (fld.values) {
              if (fld.values.constructor !== Array){
                const values = fld.values
                // take field value object (from select, checkbox, radio etc) and 
                // turn it into an array

                fld.values = JSON.parse(values).map((item)=>{
                    return {
                        id: item.id,
                        value: item.value,
                        desc: item.description
                    }
                })
              }
            }
            if(question.hasOwnProperty('type')){
              fld.type = question.type;
            } else {
              questionType(question.question_type_id, fld);
            }
            
            fieldHash[fld.id] = fld;
        }
        
    })

    // if (config.tabs) {
    //     config.tabs.forEach(function(tab) {
    //         tab.display.forEach((orderConfig) => {
    //             if ( fields[orderConfig.key] ) {
    //                 const fld = defineFieldFromConfig(tab.key, fields, orderConfig);
                    
    //                 fieldHash[fld.id] = fld
    //             }
                
    //         })
    //     })
    // } else {
    //     display.forEach(function(displayItem) {
    //         displayItem.forEach((orderConfig) => {
    //             if ( fields[orderConfig.key] ) {
    //                 const fld = defineFieldFromConfig('ALL', fields, orderConfig);
                    
    //                 fieldHash[fld.id] = fld
    //             }
                
    //         })
    //     })
        
    // }

     //console.log(fieldHash);
    return fieldHash;
    
}

function questionType(id, fld){
    axios({
        method:'get',
        url:'admin/question_types',
        headers: {'X-Requested-With': 'XMLHttpRequest', 'content-type':'application/json'}
    })
    .then(resp => {
        var question = resp.data.data.find(function (question) { return question.id === id; });
        // return question.name;
        fld.type = question.name;
    })
}

//combind field order from display/tabs and field defs from fields
// function defineFieldFromConfig(tabName, fields, orderConfig) {
//     //combind field order from display/tabs and field defs from fields
//     const fld = {
//         id: uuid(),
//         tab: tabName,
//         ...fields[orderConfig.key],
//         ...orderConfig
//     }
//     fld.label = fld.label || fld.key;

//     if (fld.format === 'number') {
//         fld.type = 'number';
        
//     }
//     if (fld.values) {
//         //take field value object (from select, checkbox, radio etc) and 
//         // turn it into an array
//         fld.values = Object.keys(fld.values).map((key)=>{
//             return {
//                 id: uuid(),
//                 value: key,
//                 desc: fld.values[key]
//             }
//         })
//     }

//     return fld;
// }

/**
 * @argument config the schema to validate, this validates the definition of a schema config objects for use with this dynamic form.
 * 
 * 
 */
export function validateSchema(config) {
    const errors = [];
    if (typeof config !== 'object') {
        errors.push("Config is not valid JSON object")
    }

    if (!config.hasOwnProperty('fields')) {
        errors.push("Missing property fields")
    } else {
        const fields = config.fields;
        if (typeof fields !== 'object') {
            errors.push("fields property should contain an object")
        }
        Object.keys(fields).forEach((fldKey) => {
            const fld = fields[fldKey];

            if (!fld.type || fld.type === '') {
                errors.push("Field '" + fldKey + "' does not have a type");
            }

            if (fld.type === 'select' || fld.type === 'radio' || fld.type === 'multicheckbox') {
                if (!fld.hasOwnProperty('values')) {
                    errors.push("Field '" + fldKey + "' is '"+fld.type+"' type but does not have a values property")
                }
            }
        })
    }

    if (!config.hasOwnProperty('display')) {
        errors.push("Missing property display")
    } else {
        config.display[0].forEach((displayConfig) => {
            const key = displayConfig.key;
            const fields = config.fields || {};
            const field = fields[key];

            if (!field) {
                errors.push("There is a display configuration for '" + key + "' but no field definition");
            }

        })
    }

    return errors;
    
}