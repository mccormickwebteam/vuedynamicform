const NU_SCHEMA = {
    "fields": {
        "q1": {
            "type": "text",
            "required": true
        },
        "q2": {
            "type": "checkbox",
            "value": "Yes"
        },
        "q3": {
            "type": "radio",
            "values": {
                "Yes": "Yes",
                "No": "No"
            }
        },
        "q4": {
            "type": "text"
        },
        "q5": {
            "type": "select",
            "values": {
                "": "",
                "Department": "Department",
                "School": "School",
                "Central": "Central"
            }
        },
        "q6": {
            "type": "select",
            "values": {
                "": "",
                "Good": "Good",
                "Better": "Better",
                "Best": "Best"
            }
        },
        "q7": {
            "type": "textarea",
            "required": false
        }
    },
    "tabs": [
        {
            "key": "t1",
            "label": "Tab 1 label",
            "display": [
                [
                    {
                        "key": "q1",
                        "label": "Question 1 label"
                    },
                    {
                        "key": "q2",
                        "label": "Question 2 label"
                    },
                    {
                        "key": "q3",
                        "label": "Question 3 label"
                    }
                ]
            ]
        },
        {
            "key": "t2",
            "label": "Tab 2 label",
            "display": [
                [
                    {
                        "key": "q4",
                        "label": "Question 4 label"
                    },
                    {
                        "key": "q5",
                        "label": "Question 5 label"
                    }
                ]
            ]
        },
        {
            "key": "t3",
            "label": "Tab 3 label",
            "display": [
                [
                    {
                        "key": "q6",
                        "label": "Question 6 label"
                    },
                    {
                        "key": "q7",
                        "label": "Question 7 label"
                    }
                ]
            ]
        }
    ]
}

const WCAS_SCHEMA = {
    "fields": {},
    "additions": {
        "fields": {
            "q3b": {
                "type": "text",
                "required": true,
                "label": "Question 3b (WCAS only)",
                "position": {
                    "tab": "t1",
                    "after": "q3"
                }
            },
            "q8": {
                "type": "text",
                "required": true,
                "label": "Question 8 (WCAS only)",
                "position": {
                    "tab": "t4"
                }
            }
        },
        "tabs": [
            {
                "key": "t4",
                "label": "Tab 4 label (WCAS only)",
                "after": "t3",
                "display": [
                    []
                ]
            }
        ]
    },
    "suppressions": {
        "fields": [
            "q4"
        ]
    },
    "tabs": []
}

const MCC_SCHEMA = {
    "fields": {},
    "additions": {
        "fields": {
            "q2b": {
                "type": "text",
                "required": true,
                "label": "Question 2b (MCC only)",
                "position": {
                    "tab": "t1",
                    "after": "q2"
                }
            }
        }
    },
    "suppressions": {
        "fields": [
            "q5"
        ]
    },
    "tabs": []
}

module.exports = {
    schemas: {
        NU: NU_SCHEMA,
        WCAS: WCAS_SCHEMA,
        MCC: MCC_SCHEMA
    },
    getBaseSchema() {
        return NU_SCHEMA;
    },
    extendSchema(base, adjustments = {}) {
        const additionalFields = adjustments.additions && adjustments.additions.fields;
        const additionalTabs = adjustments.additions && adjustments.additions.tabs;
        const suppressionFields = adjustments.suppressions && adjustments.suppressions.fields;

        return 'asfsa';
    }

}