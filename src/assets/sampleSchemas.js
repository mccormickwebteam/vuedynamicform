import MCC_JSON from './mcc_json.json'
import TABLE_SCHEMA_SAMPLE from './sample_table_schema.json'



const schemas = [{
    value: '', desc: ''
}, {
    value: JSON.stringify(TABLE_SCHEMA_SAMPLE),
    desc: 'Table Sample'
}, {
    value: '{"fields":{"commitment_type":{"type":"multicheckbox","values":{"1":"Advanced eligibility for standard research leave","2":"Guarantee three-quarters paid leave (independnet of standard accrual system)","3":"Guarantee two-quarters paid leave (independnet of standard accrual system)","4":"Guarantee one-quarter paid leave (independnet of standard accrual system)","5":"Unpaid leave","6":"Other leave commitment (detail in notes)"}},"additional_details":{"type":"multicheckbox","values":{"1":"Additional quarter(s) to be taken with on-cycle leave","2":"Back-up support for leave","3":"Good faith application(s) for external support required","4":"Option to defer back-up support","5":"Requirement to return and/or teach in next acedemic year waived","6":"Other conditions (specify in notes)"}},"number_applications":{"type":"radio","values":{"1":"1","2":"2","3":"3","U":"Unspecified","O":"Other"}}},"display":[[{"key":"commitment_type","label":"Type of Leave Commitment (select as many as apply)"},{"key":"additional_details","label":"Conditions/Additional details (select as many as apply)"},{"key":"number_applications","label":"Specify number of applications required"}]]}',
    desc: 'Leave'
}, {
    value: '{"fields":{"reduction":{"type":"multifiscal","format":"number","required":true},"release_for":{"type":"select","required":true,"values":{"":"","Self":"Self","Others":"Others"}}},"display":[[{"key":"reduction","label":"Reduction/Year"},{"key":"release_for","label":"Release for"}]]}',
    desc: "Course Reduction"
}, {
    value: '{"fields":{"description":{"type":"textarea","required":true}},"display":[[{"key":"description","label":"Description"}]]}',
    desc: "Departmental Funding Commitment"
}, {
    value: '{"fields":{"amount":{"type":"text","required":true}},"display":[[{"key":"amount","label":"Amount Per Year"}]]}',
    desc: 'Discretionary Funds'
}, {
    value: '{"fields":{"funding":{"type":"text","required":true},"previous_holders":{"type":"textarea","required":false}},"display":[[{"key":"funding","label":"Annual Research Funding"},{"key":"previous_holders","label":"Previous Chairholders"}]]}',
    desc: 'Endowed Chair'
}, {
    value: '{"fields":{"amount":{"type":"text","required":true},"purchase":{"type":"select","required":true,"values":{"":"","Yes":"Yes","No":"No"}},"forgiveness":{"type":"text","required":true},"amortization":{"type":"select","required":true,"values":{"":"","Straight Line":"Straight Line","Lump Sum":"Lump Sum"}}},"display":[[{"key":"amount","label":"Amount (lump sum)"},{"key":"purchase","label":"Requires Home Purchase"},{"key":"forgiveness","label":"Forgiveness Period (number of years)"},{"key":"amortization","label":"Amortization Method"}]]}',
    desc: 'Housing Loan'
}, {
    value: '{"fields":{"other_type":{"type":"select","required":true,"values":{"":"","Staff":"Staff","Partner":"Spousal/Partner Hire","Postdocs":"Postdocs","Research Assistants":"Research Assistants","Other":"Other"}},"details":{"type":"textarea","required":false},"source":{"type":"text","required":false}},"display":[[{"key":"other_type","label":"Type"},{"key":"details","label":"Details"},{"key":"source","label":"Source"}]]}',
    desc: 'Other'
}, {
    value: '{"fields":{"budget":{"type":"text","required":true},"relocation_type":{"type":"select","required":true,"values":{"":"","Full Billing":"Full Billing","Subsidy":"Subsidy"}}},"display":[[{"key":"budget","label":"Budget"},{"key":"relocation_type","label":"Type"}]]}',
    desc: 'Relocation'
}, {
    value: '{"fields":{"annual_amounts":{"type":"multifiscal","format":"number","required":true},"restricted":{"type":"checkbox","value":"Yes"},"renewable":{"type":"checkbox","value":"Yes"},"duration":{"type":"text"},"fund_type":{"type":"radio","values":{"Lump Sum":"Lump sum","Top up":"Top up"}},"cost_sharing":{"type":"select","values":{"":"","Department":"Department","School":"School","Central":"Central"}},"restrictions":{"type":"textarea","required":false}},"display":[[{"key":"annual_amounts","label":"Amount"},{"key":"restricted","label":"Restricted"},{"key":"renewable","label":"Renewable"},{"key":"duration","label":"Duration (years)"},{"key":"fund_type","label":"Fund type"},{"key":"cost_sharing","label":"Cost sharing"},{"key":"restrictions","label":"Restrictions"}]]}',
    desc: 'Research Funds'
}, {
    value: '{"fields":{"type":{"type":"radio","required":true,"values":{"Percentage":"Percentage Increase","Fixed":"Fixed Amount"}},"amount":{"type":"text","required":true},"effective_date":{"type":"date","required":false}},"display":[[{"key":"type","label":"Type (percentage increase guaranteed, fixed salary)"},{"key":"amount","label":"Amount"},{"key":"effective_date","label":"Effective Date"}]]}',
    desc: 'Salary'
}, {
    value: '{"fields":{"q1":{"type":"text","required":true},"q2":{"type":"checkbox","value":"Yes"},"q3":{"type":"radio","values":{"Yes":"Yes","No":"No"}},"q5":{"type":"select","values":{"":"","Department":"Department","School":"School","Central":"Central"}},"q6":{"type":"select","values":{"":"","Good":"Good","Better":"Better","Best":"Best"}},"q7":{"type":"textarea","required":false},"q3b":{"type":"text","required":true},"q8":{"type":"text","required":true}},"display":[[{"key":"q1","label":"Question 1 label"},{"key":"q2","label":"Question 2 label"},{"key":"q3","label":"Question 3 label"},{"key":"q3b","label":"Question 3b (WCAS only)"},{"key":"q4","label":"Question 4 label"},{"key":"q5","label":"Question 5 label"},{"key":"q6","label":"Question 6 label"},{"key":"q7","label":"Question 7 label"},{"key":"q8","label":"Question 8 (WCAS only)"}]],"tabs":[{"key":"t1","label":"Tab 1 label","display":[[{"key":"q1","label":"Question 1 label"}],[{"key":"q2","label":"Question 2 label"},{"key":"q3","label":"Question 3 label"},{"key":"q3b","label":"Question 3b (WCAS only)"}]]},{"key":"t2","label":"Tab 2 label","display":[[{"key":"q4","label":"Question 4 label"}],[{"key":"q5","label":"Question 5 label"}]]},{"key":"t3","label":"Tab 3 label","display":[[{"key":"q6","label":"Question 6 label"},{"key":"q7","label":"Question 7 label"}]]},{"key":"t4","label":"Tab 4 label (WCAS only)","display":[[{"key":"q8","label":"Question 8 (WCAS only)"}]]}]}',
    desc: 'WCAS'
}, {
    desc: "MCC Sample",
    value: JSON.stringify(MCC_JSON)
}];

/**
 * 
 *              checkbox: 'checkbox',
                date: 'datefield',
                multicheckbox: 'multicheckbox',
                multiyear: 'multiyear',
                multifiscal: 'multifiscal',
                notefield: 'notefield',
                radio: 'radio',
                select: 'combo',
                table: 'dynamic-table',
                text: 'input',
                textarea: 'wit-textarea',
                unknown: 'unknown',
                upload: 'upload'
 */

let schemaConfig;

schemaConfig = {
    fields: {
        "f1": {
            "type":"checkbox",
            label: "Checkbox Type"
        }
    },
    display: [[{
        key: "f1"
    }]]
}
schemas.push({
    value: JSON.stringify(schemaConfig),
    desc: 'Checkbox'
})


schemaConfig = {
    fields: {
        "f1": {
            "type":"date",
            label: "Date Type"
        }
    },
    display: [[{
        key: "f1"
    }]]
}
schemas.push({
    value: JSON.stringify(schemaConfig),
    desc: 'Date'
})


schemaConfig = {
    fields: {
        "f1": {
            "type":"richtext",
            label: "Rich Text"
        }
    },
    display: [[{
        key: "f1"
    }]]
}
schemas.push({
    value: JSON.stringify(schemaConfig),
    desc: 'Rich Text Type'
})

schemaConfig = {
    fields: {
        "f1": {
			"type":"text",
			"format": "number",
			label: "Text with Number Type",
			precision: 1
		},
		"f2": {
			"type":"text",
			"format": "number",
			label: "Text with Number Type",
			"minvalue": 0,
			"maxvalue": 15,
			precision: 2
		},
		"f4": {
			"type":"number",
			label: "Text with Number Type",
			"required": true,
			precision: 3
		},
		"f3": {
			"type":"text",
			"format": "number",
            label: "Text with Number Type"
        }
    },
    display: [[{
        key: "f1"
    }, {
        key: "f2"
    }, {
        key: "f3"
    }, {
        key: "f4"
    }]]
}
schemas.push({
    value: JSON.stringify(schemaConfig),
    desc: 'Numeric Field'
})

schemaConfig = {
	"fields": {
		
		"bio_first_name": {
			"type": "text",
            "label": "First Name",
            "required": true,
            maxlength: 30,
            minlength:3
		},
		"bio_middle_name": {
			"type": "text",
            "label": "Middle Name",
            required: false
		},
		"bio_appointment": {
			"type": "text",
            "label": "Appointment(s)",
            required: true
		},
		"bio_date_started_nu": {
			"type": "date",
            "label": "Date Started NU",
            required: true
		},
		"bio_photo": {
			"type": "upload",
			"label": "Upload New Photo"
		},
		"bio_statement": {
			"type": "textarea",
			"label": "Personal Bio (not to exceed 250 words)",
            "maxlength": 250,
            required: true
        },
        "bio_statement2": {
			"type": "textarea",
			"label": "Personal Bio (OK to exceed 250 words)"
		},
		"cv_notes": {
			"type": "notefield",
			"value": "Faculty are requested to upload an extended CV for internal review. An alternate CV, available to the public, may be uploaded for the McCormick website",
			"label": "Curriculum Vitae"
		},
		"cv_upload_internal": {
			"type": "upload",
			"label": "Upload extended CV for internal review (mandatory)"
		},
		"cv_upload_external": {
			"type": "upload",
			"label": "Upload alternate CV for McCormick website"
		},
		"sa_notes": {
			"type": "notefield",
			"header": true,
			"label": "Self-Assessment",
			"value": "Faculty are to report their achievements for the past year, expected accomplishments for the coming year and enter a brief plan for the next five years.\n\nSection 1 - Achievements\nDescribe AY15-16 teaching, research and other accomplishments. Describe your goals and achievements for this past academic year.\n\nSection 2 - Expectations\nDescribe AY15-16 teaching and research goals. Describe what you expect to accomplish during the coming academic year.\n\nSection 3 - Five Year Plan\nDescribe your five year career plan.\n\nFormat: The three areas of reporting have five sections into which responses may be entered (each limited to 300 character narratives). Responses from the previous year are available by clicking below each section."
		},
		"achievements_note": {
			"type": "notefield",
			"value": "(If you were on an academic leave for any portion of the past academic year, explicitly note accomplishments related to the leave and value returned to you and Northwestern)\nPlease state your responses in up to five bullet points of less than 300 characters each"
		},
		"achievement1": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"achievement2": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"achievement3": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"achievement4": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"achievement5": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"accomplish_note": {
			"type": "notefield",
			"format": "text",
			"required": false,
			"value": "Please state your response in up to five bullet points of less than 300 characters each"
		},
		"accomplish1": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"accomplish2": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"accomplish3": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"accomplish4": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"accomplish5": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"plan_note": {
			"type": "notefield",
			"format": "text",
			"required": false,
			"value": "Please state your response in up to five bullet points of less than 300 characters each"
		},
		"plan1": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"plan2": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"plan3": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"plan4": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"plan5": {
			"type": "text",
			"required": false,
			"maxlength": 300
		},
		"teaching_info": {
			"type": "notefield",
			"label": "Teaching",
			"header": true,
			"value": "Information about courses taught is downloaded from SES. Any discrepancies should be reported to webdev@mccormick.northwestern.edu"
		},
		"classes_taught": {
			"type": "table",
			"label": "Courses Taught"
		},
		"classes_missed": {
			"type": "text",
			"label": "Indicate classes missed due to travel"
		},
		"undergraduate_taught": {
			"type": "notefield",
			"label": "Number of undergraduate students advised",
			"value": "DATA SOURCE: Auto populated from MAS"
		},
		"funding_header": {
			"type": "notefield",
			"header": true,
			"label": "Funding"
		},
		"salary_recovery_note": {
			"type": "notefield",
			"label": "Academic Salary Recovery",
			"value": "This information will be entered by your business administrator"
		},
		"salary_recovery": {
			"type": "text",
			"label": "% Academic salary recovered in 2016-2017",
			"tooltip": "This information will be entered by your business administrator. Please contact your Business Administrator regarding discrepancies."
		},
		"sponsored_research_note": {
			"type": "notefield",
			"label": "Sponsored Research",
			"value": "Data is auto-populated from InfoEd and NU Financials. Clicking on a grant will reveal its detailed investigator information in the section titled \"Sponsored Research - Investigator\". This will appear to the right of or below the main section titled \"Sponsored Research - Basic Information\" (depending on your screen resolution)."
		},
		"sponsored_research_basic": {
			"type": "table",
			"label": "Sponsored Research - Basic Information"
		},
		"sponsored_research_invest": {
			"type": "table",
			"label": "Sponsored Research - Investigator"
		},
		"research_proposals": {
			"type": "notefield",
			"label": "Research Proposals Submitted",
			"value": "Data is auto-populated from InfoEd."
		},
		"publications_note": {
			"type": "notefield",
			"header": true,
			"label": "Publications"
		},
		"publications": {
			"type": "notefield",
			"label": "Refereed and Archival Publications - NU Scholars",
			"value": "Refereed and Archival Publications are automatically downloaded from NU Scholars. This section of the Annual Report will be locked and not subject to edit for uniformity of reporting."
		},
		"other_publications": {
			"type": "textarea",
			"label": "Other Refereed Archival Publications"
		},
		"book_contributions": {
			"type": "textarea",
			"label": "Books and Book Chapters"
		},
		"h_index": {
			"type": "textarea",
			"label": "Web of Science, Google Scholar and NU Scholars will be included in the Annual Report. Faculty will also have the oppurtunity to self-report their H-Index."
		},
		"issued_patents": {
			"type": "textarea",
			"label": "Issued Patents and Software"
		},
		"publications_warning": {
			"type": "notefield",
			"label": "*NOTE",
			"value": "Non-referedd publications, lectures, talks, posters, abstracts and conference proceedings should be listed in the CV only. The uploaded CB will be used as the definitive source for this information - it is not necassary to duplicate online. the CB is part of the Annual Report."
		}
	},
	"tabs": [
		{
			"key": "profile",
			"label": "Biographical and CV",
			"display": [
				
				{
					"key": "bio_first_name"
				},
				{
					"key": "bio_middle_name"
                },
                {
					"key": "bio_last_name"
				},
				{
					"key": "bio_appointment"
				},
				{
					"key": "bio_date_started_nu"
				},
				{
					"key": "bio_photo"
				},
				{
					"key": "bio_statement"
				},
				{
					"key": "bio_statement2"
				},
				{
					"key": "cv_notes"
				},
				{
					"key": "cv_upload_internal"
				},
				{
					"key": "cv_upload_external"
				}
			]
		},
		{
			"key": "assessment",
			"label": "Self Assessment",
			"display": [
				{
					"key": "sa_notes"
				},
				{
					"key": "achievements_note",
					"label": "Achievements for the past academic year"
				},
				{
					"key": "achievement1",
					"label": "Achievement 1"
				},
				{
					"key": "achievement2",
					"label": "Achievement 2"
				},
				{
					"key": "achievement3",
					"label": "Achievement 3"
				},
				{
					"key": "achievement4",
					"label": "Achievement 4"
				},
				{
					"key": "achievement5",
					"label": "Achievement 5"
				},
				{
					"key": "accomplish_note",
					"label": "Expecting to accomplish during the coming academic year"
				},
				{
					"key": "accomplish1",
					"label": "Accomplishment 1"
				},
				{
					"key": "accomplish2",
					"label": "Accomplishment 2"
				},
				{
					"key": "accomplish3",
					"label": "Accomplishment 3"
				},
				{
					"key": "accomplish4",
					"label": "Accomplishment 4"
				},
				{
					"key": "accomplish5",
					"label": "Accomplishment 5"
				},
				{
					"key": "plan_note",
					"label": "Briefly describe your plan for the direction of your career over the next 5 years"
				},
				{
					"key": "plan1",
					"label": "Direction 1"
				},
				{
					"key": "plan2",
					"label": "Direction 2"
				},
				{
					"key": "plan3",
					"label": "Direction 3"
				},
				{
					"key": "plan4",
					"label": "Direction 4"
				},
				{
					"key": "plan5",
					"label": "Direction 5"
				}
			]
		},
		{
			"key": "supervision",
			"label": "Teaching",
			"display": [
				{
					"key": "teaching_info"
				},
				{
					"key": "classes_taught"
				},
				{
					"key": "classes_missed"
				},
				{
					"key": "undergraduate_taught"
				}
			]
		},
		{
			"key": "funding",
			"label": "Funding",
			"display": [
				{
					"key": "funding_header"
				},
				{
					"key": "salary_recovery_note"
				},
				{
					"key": "salary_recovery"
				},
				{
					"key": "sponsored_research_note"
				},
				{
					"key": "sponsored_research_basic"
				},
				{
					"key": "sponsored_research_invest"
				},
				{
					"key": "research_proposals"
				}
			]
		},
		{
			"key": "service",
			"label": "Professional Activity",
			"display": [
				{
					"key": "publications_note"
				},
				{
					"key": "publications"
				},
				{
					"key": "other_publications"
				},
				{
					"key": "book_contributions"
				},
				{
					"key": "h_index"
				},
				{
					"key": "issued_patents"
				},
				{
					"key": "publications_warning"
				}
			]
		}
	],
	"display": [
		[
			{
				"key": "bio_header"
			},
			{
				"key": "bio_info"
			},
			{
				"key": "bio_first_name"
			},
			{
				"key": "bio_middle_name"
			},
			{
				"key": "bio_appointment"
			},
			{
				"key": "bio_date_started_nu"
			},
			{
				"key": "bio_photo"
			},
			{
				"key": "bio_statement"
			},
			{
				"key": "cv_notes"
			},
			{
				"key": "cv_upload_internal"
			},
			{
				"key": "cv_upload_external"
			},
			{
				"key": "sa_notes"
			},
			{
				"key": "achievements_note",
				"label": "Achievements for the past academic year"
			},
			{
				"key": "achievement1",
				"label": "Achievement 1"
			},
			{
				"key": "achievement2",
				"label": "Achievement 2"
			},
			{
				"key": "achievement3",
				"label": "Achievement 3"
			},
			{
				"key": "achievement4",
				"label": "Achievement 4"
			},
			{
				"key": "achievement5",
				"label": "Achievement 5"
			},
			{
				"key": "accomplish_note",
				"label": "Expecting to accomplish during the coming academic year"
			},
			{
				"key": "accomplish1",
				"label": "Accomplishment 1"
			},
			{
				"key": "accomplish2",
				"label": "Accomplishment 2"
			},
			{
				"key": "accomplish3",
				"label": "Accomplishment 3"
			},
			{
				"key": "accomplish4",
				"label": "Accomplishment 4"
			},
			{
				"key": "accomplish5",
				"label": "Accomplishment 5"
			},
			{
				"key": "plan_note",
				"label": "Briefly describe your plan for the direction of your career over the next 5 years"
			},
			{
				"key": "plan1",
				"label": "Direction 1"
			},
			{
				"key": "plan2",
				"label": "Direction 2"
			},
			{
				"key": "plan3",
				"label": "Direction 3"
			},
			{
				"key": "plan4",
				"label": "Direction 4"
			},
			{
				"key": "plan5",
				"label": "Direction 5"
			},
			{
				"key": "teaching_info"
			},
			{
				"key": "classes_taught"
			},
			{
				"key": "classes_missed"
			},
			{
				"key": "undergraduate_taught"
			},
			{
				"key": "funding_header"
			},
			{
				"key": "salary_recovery_note"
			},
			{
				"key": "salary_recovery"
			},
			{
				"key": "sponsored_research_note"
			},
			{
				"key": "sponsored_research_basic"
			},
			{
				"key": "sponsored_research_invest"
			},
			{
				"key": "research_proposals"
			},
			{
				"key": "publications_note"
			},
			{
				"key": "publications"
			},
			{
				"key": "other_publications"
			},
			{
				"key": "book_contributions"
			},
			{
				"key": "h_index"
			},
			{
				"key": "issued_patents"
			},
			{
				"key": "publications_warning"
			}
		]
	]
}


schemas.push({
    value: JSON.stringify(schemaConfig),
    desc: 'Many Types'
})

export default schemas