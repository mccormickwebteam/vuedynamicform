const conf = ['key', 'type', 'format', 'required', 'values', 'label', 'tooltip']

export default {
    "WCAS": {
        "fields": {
            "q1": {
                "type": "text",
                "required": true
            },
            "q2": {
                "type": "checkbox",
                "value": "Yes"
            },
            "q3": {
                "type": "radio",
                "values": {
                    "Yes": "Yes",
                    "No": "No"
                }
            },
            "q5": {
                "type": "select",
                "values": {
                    "": "",
                    "Department": "Department",
                    "School": "School",
                    "Central": "Central"
                }
            },
            "q6": {
                "type": "select",
                "values": {
                    "": "",
                    "Good": "Good",
                    "Better": "Better",
                    "Best": "Best"
                }
            },
            "q7": {
                "type": "textarea",
                "required": false
            },
            "q3b": {
                "type": "text",
                "required": true
            },
            "q8": {
                "type": "text",
                "required": true
            }
        },
        "display": [
                {
                    "key": "q1",
                    "label": "Question 1 label"
                },
                {
                    "key": "q2",
                    "label": "Question 2 label"
                }, {
                    "key": "q3",
                    "label": "Question 3 label"
                },
                {
                    "key": "q3b",
                    "label": "Question 3b (WCAS only)"
                },
                {
                    "key": "q4",
                    "label": "Question 4 label"
                },
                {
                    "key": "q5",
                    "label": "Question 5 label"
                },
                {
                    "key": "q6",
                    "label": "Question 6 label"
                },
                {
                    "key": "q7",
                    "label": "Question 7 label"
                },
                {
                    "key": "q8",
                    "label": "Question 8 (WCAS only)"
                }
        ],
        "tabs": [
            {
                "key": "t1",
                "label": "Tab 1 label",
                "display": [
                    [
                        {
                            "key": "q1",
                            "label": "Question 1 label"
                        }
                    ], [
                        {
                            "key": "q2",
                            "label": "Question 2 label"
                        }, {
                            "key": "q3",
                            "label": "Question 3 label"
                        },
                        {
                            "key": "q3b",
                            "label": "Question 3b (WCAS only)"
                        }
                    ]
                ]
            },
            {
                "key": "t2",
                "label": "Tab 2 label",
                "display": [
                    [
                        {
                            "key": "q4",
                            "label": "Question 4 label"
                        }
                    ], [
                        {
                            "key": "q5",
                            "label": "Question 5 label"
                        }
                    ]
                ]
            },
            {
                "key": "t3",
                "label": "Tab 3 label",
                "display": [
                    [
                        {
                            "key": "q6",
                            "label": "Question 6 label"
                        },
                        {
                            "key": "q7",
                            "label": "Question 7 label"
                        }
                    ]
                ]
            },
            {
                "key": "t4",
                "label": "Tab 4 label (WCAS only)",
                "display": [
                    [
                        {
                            "key": "q8",
                            "label": "Question 8 (WCAS only)"
                        }
                    ]
                ]
            }
        ]
    },
    "MCC": {
    "fields": {
        "q1": {
            "type": "text",
            "required": true
        },
        "q2": {
            "type": "checkbox",
            "value": "Yes"
        },
        "q3": {
            "type": "radio",
            "values": {
                "Yes": "Yes",
                "No": "No"
            }
        },
        "q4": {
            "type": "text"
        },
        "q6": {
            "type": "select",
            "values": {
                "": "",
                "Good": "Good",
                "Better": "Better",
                "Best": "Best"
            }
        },
        "q7": {
            "type": "textarea",
            "required": false
        },
        "q2b": {
            "type": "text",
            "required": true
        }
    },
    "tabs": [
        {
            "key": "t1",
            "label": "McCormick: Tab 1 label",
            "display": [
                [
                    {
                        "key": "q1",
                        "label": "Question 1 label"
                    },
                    {
                        "key": "q2",
                        "label": "Question 2 label"
                    },
                    {
                        "key": "q3",
                        "label": "Question 3 label"
                    },
                    {
                        "key": "q2b",
                        "label": "Question 2b (MCC only)"
                    }
                ]
            ]
        },
        {
            "key": "t2",
            "label": "Tab 2 label",
            "display": [
                [
                    {
                        "key": "q4",
                        "label": "Question 4 label"
                    },
                    {
                        "key": "q5",
                        "label": "Question 5 label"
                    }
                ]
            ]
        },
        {
            "key": "t3",
            "label": "Tab 3 label",
            "display": [
                [
                    {
                        "key": "q6",
                        "label": "Question 6 label"
                    },
                    {
                        "key": "q7",
                        "label": "Question 7 label"
                    }
                ]
            ]
        }
    ]
}

}