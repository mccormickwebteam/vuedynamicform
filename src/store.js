import Vuex from 'vuex'
import Vue from 'vue'
import uuid from 'uuid/v4'

import Config from './assets/sampleConfig';

Object.keys(Config).map(key => {
    Config[key].display = flatFields(Config[key]);
})

function flatFields(config) {
   // const display = config.display || [];
    //fields holds some more definition of the fields.
   // const fields = config.fields;
    const flatFields = [];
    const fieldHash = {}

    // if (config.tabs) {
    //     config.tabs.forEach(function(tab) {
    //         display.forEach((orderConfig) => {
    //             if ( fields[orderConfig.key] ) {
        
    //                 //combind field order from display/tabs and field defs from fields
    //                 const fld = {
    //                     id: uuid(),
    //                     ...fields[orderConfig.key],
    //                     label: orderConfig.label || orderConfig.key,
    //                     key: orderConfig.key,
    //                     tab: tab.key
    //                 }
        
    //                 if (fld.values) {
    //                     //take field value object (from select, checkbox, radio etc) and 
    //                     // turn it into an array
    //                     fld.values = Object.keys(fld.values).map((key)=>{
    //                         return {
    //                             id: uuid(),
    //                             value: key,
    //                             desc: fld.values[key]
    //                         }
    //                     })
    //                 }
    //                 fieldHash[fld.id] = fld
    //             }
                
    //         })
    //     })
    // } else {
        config.data.forEach((question) => {
            if ( question.active ) {
                
                const fld = {
                    id: uuid(),
                    type: question.questionType,
                    required: question.required,
                    label: question.name,
                    key: question.name,
                    values: question.options,
                    maxlength: question.maximum_length,
                    minlength: question.minimum_length
                }
    
                if (fld.values) {
                    //take field value object (from select, checkbox, radio etc) and 
                    // turn it into an array
                    fld.values = fld.values.map((item)=>{
                        return {
                            id: uuid(),
                            value: item.value,
                            desc: item.description
                        }
                    })
                }
                fieldHash[fld.id] = fld
            }
            
        })
    // }
    
    return fieldHash;
}

export default {
    strict: true,

    state: {
        selectedConfigName: "WCAS",
        config: Config.WCAS,
        configs: Config,
        currentEditField: null
    },

    getters: {
        configOptions: function(state) {
            //Combo wants options as {VAL: DESC}
            // should change to support more options because using the config
            // as a string value is possible but weird.
            return Object.keys(state.configs).reduce((result, name) => {
                result[name] = name;
                return result;
            }, {});
        },
        
        fieldTypeOptions: function(state) {
            return {
                text: "Text",
                checkbox: "Checkbox",
                multifiscal: "Multi Fiscal",
                radio: "Radio",
                select: "Select",
                textarea: "Text Area"
            }
        },
        getCurrentEditField: function(state) {
            if (state.currentEditField) {
                return state.config.display[state.currentEditField]
            }
        },
        orderedFields: function(state) {
            var fields = [];
            
            for (let id in state.config.display) {
                fields.push(state.config.display[id])
            }
            return fields
        },
        fieldCount: function(state) {
            return Object.keys(state.config.display).length;
        }
    },
    mutations: {
        addField: function(state) {
            const newId = uuid();
            Vue.set(state.config.display, newId, {
                id: newId,
                type: 'text'
            })
        },
        
        changeActiveConfig: function(state, configName) {
            if (state.configs[configName]) {
                state.config = state.configs[configName]
                state.selectedConfigName = configName
            }
        },
        deleteField: function(state, field) {
            Vue.delete(state.config.display, field.id)
        },


        updateFieldDefinition: function(state, field) {
            state.config.display[field.id] = {...field};
        },
        moveFieldItem: function(state, {dir, item}) {
            const fieldKeys = Object.keys(state.config.display)
            const fieldIndex = fieldKeys.indexOf(item.id)
            const newOrder = {}
            
            if (fieldIndex !== -1) {
                let move = -1;
                if (dir === 'down') {
                    move = 1
                }
                fieldKeys.splice(fieldIndex + move, 0, fieldKeys.splice(fieldIndex, 1)[0])
            }

            fieldKeys.forEach(k => newOrder[k] = state.config.display[k])

            state.config.display = newOrder
        },

        // methods for working with values in the modal window to edit value choices for a 
        // particular field.
        addValueEditValues: function(state, field) {
            const stateField = state.config.display[field.id]
            stateField.editValues.push({
                id: uuid(),
                value: '',
                desc: ''
            });
            
            state.config.display[field.id] =stateField;
        },
        clearEditField: function(state) {
            if (state.currentEditField) {
                const field = state.config.display[state.currentEditField]
                field.editValues = undefined
            }
            
            state.currentEditField = undefined
        },
        deleteEditValueItem: function(state, item) {
            if (state.currentEditField) {
                const field = state.config.display[state.currentEditField]
                
                const editValues = field.editValues;
                editValues.splice(editValues.indexOf(item), 1);
            }
            
        },
        moveFieldValueItem: function(state, {dir, item}) {
            if (state.currentEditField) {
                const field = state.config.display[state.currentEditField]
                const editValues = field.editValues
                const fieldIndex = editValues.indexOf(item)
                
                if (fieldIndex !== -1) {
                    let move = -1;
                    if (dir === 'down') {
                        move = 1
                    }
                    editValues.splice(fieldIndex + move, 0, editValues.splice(fieldIndex, 1)[0])
                }

            }
        },
        saveEditFieldValues: function(state, field) {
            
            field.values = field.editValues.map(v => {
                return {...v} 
            })
        
            state.currentEditField = undefined
        },
        setFieldForEdit: function(state, field) {
            if (field) {
                state.currentEditField = field.id
                field.editValues = (field.values || []).map(v => {
                    return {...v} 
                })
            } else {
                state.currentEditField = undefined
            }
        }
    }
}