# form-builder

> Dynamic Form and Configurations

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how the build process works for this package, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).


## Install
``` bash
npm install WeinbergIT/VueDynamicForm --save-dev
```
``` javascript
import DynamicForm from 'vue-dynamic-form'

Vue.component('my-app-component', {
    components: {
        DynamicForm
    }
});
```

# Usage
``` html
<script>
var state = {
    schema: {...},
    values: {...},
    readOnly: false
}
</script>
<dynamic-form 
	:schema="state.schema"
	:values="state.values"
    :readOnly="state.readOnly"
    :apiParams="state.apiParams"
>
</dynamic-form>
```

## Dynamic Form Configuration
| Property | Description |
|----------|----------------|
|schema|Object with schema configuration|
|activeTab|The 'key' of the tab to set current|
|apiParams|Params to include that are referenced by fields (table type url to call for instance)|
|values|The current values in the form.|

## Dynamic Form Events
| Event | Description | Parameters |
|----------|----------------|--|
|input|Fires when form values change|Form Values|
|validationchange|Fires when form values change|Validation Object|

## Schema Configuration
| Property | Description |
|----------|----------------|
|fields|Object with field keys as properties and field configuration objects|
|tabs|Array of Objects with tab configurations that include key, label and display. Takes precedent over below display config|
|display|Object[] of Display Configurations. Ignored if tabs is set|

## Field Config
| Property | Description|Type|Default|
|----------|------------|----|-----|
|type|Field Type|string|'text'|
|label|Label|string|''|
|value|Value|Any|null|
|values|Values for combos/radio/multi checkbox etc.|null|
|maxlength|Max Length|number|null|
|minlength|Min Length|number|null|
|required|Required|boolean|false|

## Display Config Array
Array of display configurations. The order of the array indicates the order of display
### Display Config Properties*
|Property|Description|
|--------|-----------|
|key|The key that references an item in the Field Configuration Object|
|label|(Optional) Override the label defined in the Field Configuration|
*Any property in the field definition can be overridden in the display array.

## Field Types
|Type|Extra Configs|
|-|-|
|checkbox||
|date||
|multicheckbox|values: [{key: 'label'}...{}]|
|multifiscal||
|multiyear||
|notefield|header: makes title large<br>value: body of notefield|
|number|minvalue, maxvalue|
|radio|values: [{key: 'label'}...{}]|
|select|values: [{key: 'label'}...{}]|
|table|title: table title<br>columns: column definitions<br>data: rows of data<br>apiUrl: the url to request when the component is mounted<br>dataPath: the access path for the table records with data properties that match the column definitions keys ('json.data.customers')<br>apiParamsWhitelist: String[] of params to pass from the apiParams object which is shared with all fields by passing it into the DynamicForm from the parent app|
|text|minlength<br>maxlength|
|textarea|minlength<br>maxlength|
|upload||
